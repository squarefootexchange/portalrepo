﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestPortalPro.Startup))]
namespace TestPortalPro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
