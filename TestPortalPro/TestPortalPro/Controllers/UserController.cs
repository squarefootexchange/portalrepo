﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using TestPortalPro.Models;

namespace TestPortalPro.Controllers
{
    public class UserController : Controller
    {
        directsq_proportalEntities db = new directsq_proportalEntities();
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccess]
        public ActionResult userDashboard(signup usrProfile)
        {
            signup userProfile = new signup();

            if (Session["UserID"] != null)
            {
                //var _context = new directsq_proportalEntities();
                //var projectlist = db.new_project.Select(e => new new_project
                //{
                //    project_name = e.project_name
                //}
                //).ToList();

                List<new_project> ObjEmp = new List<new_project>();
              

                try
                {

                    //ObjEmp = db.signups.Where(x => x.status == 1010 && x.delined != "active").ToList();
                    //db.signups.Where(x => x.agentid == 1);
                    //ObjEmp = db.new_project.Select(x => x.project_name).ToList();
                    ViewBag.Message = db.new_project.Select(x => x.project_name).ToList();

                    ViewBag.Message = db.new_project.Select(x => x.project_name).Distinct();
                    ViewBag.category = db.new_project.Select(x => x.project_category).Distinct().ToList();
                    ViewBag.size = db.new_project.Select(x => x.plot_size).Distinct().ToList();
                    ViewBag.view = db.new_project.Select(x => x.plot_view).Distinct().ToList();

                }



                catch (Exception ex)
                {
                    throw ex;
                }
                //db.signups.Where(x => x.status == 0001)
               

                //return View();


                var agentid = Session["UserID"];
                var email = Session["UserName"];

                userProfile = db.signups.Where(x => x.email == email).FirstOrDefault();

                return View(userProfile);

            }
            else
            {
                return RedirectToAction("Login");
            }



           // return View();
        }
        [NoDirectAccess]
        public ActionResult userAccount()
        {
            return View();
        }
        [NoDirectAccess]
        public ActionResult bookProperty()
        {
             return View();
        }

        [NoDirectAccess]
        public ActionResult deleteUser()
        {
            return View();
        }

        [NoDirectAccess]
        public ActionResult projectInfo()
        {
            try
            {
                ViewBag.Message = db.new_project.Select(x => x.project_name).Distinct();
                ViewBag.category = db.new_project.Select(x => x.project_category).Distinct().ToList();
            }



            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }


        [HttpPost]
        public JsonResult userData()
        {
            List<signup> ObjEmp = new List<signup>();

            try
            {
                ObjEmp = db.signups.Where(x => x.status == 1010 && x.delined != "active").ToList();
                //db.signups.Where(x => x.agentid == 1);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            //db.signups.Where(x => x.status == 0001)
            return Json(ObjEmp, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public JsonResult userDelData()
        {
            List<usr_login> ObjEmp = new List<usr_login>();
            
            try
            {
                ObjEmp = db.usr_login.ToList();
                //db.signups.Where(x => x.agentid == 1);
            }

            catch (Exception ex)
            {
                throw ex;
            }
            //db.signups.Where(x => x.status == 0001)
            return Json(ObjEmp, JsonRequestBehavior.AllowGet);

        }

        public JsonResult approveUser(long agentid, string fname, string lname, string email, signup usr)
        {
            List<usr_login> user = new List<usr_login>();
            List<signup> ObjEmp = new List<signup>();

            try
            {
                var passList = db.signups.Where(x => x.email == email && x.agentid == agentid).Select(x => new { x.password }).ToList();
                string pass = passList[0].ToString();
                char[] lst = { '=',' ' };
                String[] ps = pass.Split(lst);
                usr.delined = "active";
                
                
                db.usr_login.Add(new Models.usr_login
                {
                    agentid = agentid,
                    email = email,
                    password =  ps[4],                   
                    user_role = "User"
                });   
               
                var delind = db.signups.Where(x => x.email == email).FirstOrDefault();
                delind.delined = usr.delined;
                db.SaveChanges();             

                string mailSubj = "Square Foot Exchange";
                string mailBody = "Hi " + fname + " " + " " + " " + lname + " , <br/> <br/> Welcome Square Foot Exchange Portal.Your account is APPROVED..<br/> <br/> <a>Click here to login</a>";


                SendEmail(email, mailSubj, mailBody);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(user);
        }

        
        public JsonResult updateProfile(string fname, string lname, string email, string cnic, string city, string dob, string profession)
        {
            try
            {
                ViewBag.Message = "Your Profile Has been Updated!!";
                var update = db.signups.Where(x => x.email == email).FirstOrDefault();
                update.fname = fname;
                update.lname = lname;
                update.email = email;
                update.cnic = cnic;
                update.city = city;
                update.dob = dob;
                update.profession = profession;
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(ViewBag.Message);
        }



        public JsonResult disapproveUser(long agentid, string fname, string lname, string email)
        {
            List<usr_login> user = new List<usr_login>();
            List<signup> ObjEmp = new List<signup>();

            try
            {
                using (directsq_proportalEntities Context = new directsq_proportalEntities())
                {

                    Context.signups.Remove(Context.signups.Single(a => a.email == email));
                    Context.SaveChanges();
                }

                string mailSubj = "Square Foot Exchange";
                string mailBody = "Hi " + fname + " " + " " + " " + lname + " , <br/> <br/> Welcome Square Foot Exchange Portal.Your account is reject due to some problems better luck next time..<br/> <br/> <a>Click here to login</a>";

                SendEmail(email, mailSubj, mailBody);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(user);
        }


        public JsonResult DelUser(long agentid, string email)
        {
            List<usr_login> user = new List<usr_login>();
            List<signup> ObjEmp = new List<signup>();

            try
            {
                using (directsq_proportalEntities Context = new directsq_proportalEntities())
                {

                    Context.usr_login.Remove(Context.usr_login.Single(a => a.email == email));
                    Context.SaveChanges();
                }

                //string mailSubj = "Square Foot Exchange";
                //string mailBody = "Hi " + fname + " " + " " + " " + lname + " , <br/> <br/> Welcome Square Foot Exchange Portal.Your account is reject due to some problems better luck next time..<br/> <br/> <a>Click here to login</a>";

                //SendEmail(email, mailSubj, mailBody);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(user);
        }

        public static void SendEmail(string email, string mailSubj, string mailBody)
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(email);
            //mail.To.Add("Another Email ID where you wanna send same email");
            mail.From = new MailAddress(email);
            mail.Subject = mailSubj; //"Square Foot Exchange";

            mail.Body = mailBody;//"Hi " + fname + " " + " " + " " + lname + " , <br/> <br/> Welcome Square Foot Exchange Portal.Your account is approved..<br/> <br/> <a>Click here to login</a>";

            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            smtp.Credentials = new System.Net.NetworkCredential
            ("squarefootexchange2019@gmail.com", "spoof@808");
            //Or your Smtp Email ID and Password
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }

        public JsonResult changePassword(string currentpass, string newpass)
        {
            var agentid = Session["UserID"];
            var email = Session["UserName"];

            try
            {
                var currpass = db.usr_login.Where(x => x.email == email).Select(x => x.password).FirstOrDefault();
                if (currentpass == currpass)
                {
                    ViewBag.Message = "Your password is changed";
                    var pass = db.usr_login.Where(x => x.email == email).FirstOrDefault();
                    pass.password = newpass;
                    db.SaveChanges();
                }
                else
                {
                    ViewBag.Message = "Current Password is Incorrect!!";
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
           
            return Json(ViewBag.Message);
        }

    }

   
}