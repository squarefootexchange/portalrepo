﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestPortalPro.Models;

namespace TestPortalPro.Controllers
{
    public class AdminController : Controller
    {
        directsq_proportalEntities db = new directsq_proportalEntities();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccess]
        public ActionResult Admin()
        {
            return View();
        }
        [NoDirectAccess]
        public ActionResult PropertyManagement()
        {
            return View();
        }
        [NoDirectAccess]
        public ActionResult NewProject()
        {
            return View();
        }
        [NoDirectAccess]
        public ActionResult NewProperty()
        {
            return View();
        }

        [HttpPost]
        public JsonResult addProject(string[] data,string[] number)
        {
            try { 
            string row;
            for(var i = 0; i < number.Length; i++)
            {
                row = number[i].ToString();
                char[] lst = { '-' };
                String[] ps = row.Split(lst);

                db.new_project.Add(new Models.new_project
                {                    
                    project_name = ps[1],
                    project_category = ps[2],
                    plot_size = Int32.Parse(ps[3]),
                    number_plot = Int32.Parse(ps[5]),
                    plot_view = ps[4]
                });
                    db.SaveChanges();
                    return Json(1);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(0);
            
        }

        [HttpPost]

        public JsonResult getProjectName(new_project project)
        {
            List<new_project> prjnam = new List<new_project>();
            List<string> prjname = new List<string>();

            try
            {
                prjname = db.new_project.Select(x => x.project_name).ToList();
                
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return Json(prjname, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getProjectCategory(string prjname)
        {

            List<string> prjctgry = new List<string>();

            try
            {
                prjctgry = db.new_project.Where(x => x.project_name == prjname).Select(x => x.project_category).ToList();

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return Json(prjctgry, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getPlotSize(string prjctgry, string prjname)
        {
            List<int> plotsize = new List<int>();

            try
            {
                plotsize = db.new_project.Where(x => x.project_name == prjname && x.project_category == prjctgry ).Select(x => x.plot_size).ToList();

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return Json(plotsize, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult getPlotView(string prjctgry, string prjname, int plotsize)
        {
            List<string> plotview = new List<string>();

            try
            {
                plotview = db.new_project.Where(x => x.project_name == prjname && x.project_category == prjctgry && x.plot_size == plotsize).Select(x => x.plot_view).ToList();

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return Json(plotview, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult getNumberofPlots(string prjctgry, string prjname, int plotsize, string plotview)
        {
            List<int> numberofplots = new List<int>();

            try
            {
                numberofplots = db.new_project.Where(x => x.project_name == prjname && x.project_category == prjctgry && x.plot_size == plotsize && x.plot_view == plotview).Select(x => x.number_plot).ToList();

            }

            catch (Exception ex)
            {
                throw ex;
            }

            return Json(numberofplots, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public JsonResult UploadFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/Content/UploadImages"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        [HttpPost]
        public JsonResult savePropertyDB(string prjname, string prjctgry, int plotsize, string plotview, int numberofplots, string details, int price)
        {
            List<new_property> newproperty = new List<new_property>();
            try
            {
                var prjid = db. new_project.Where(x => x.project_name == prjname && x.project_category == prjctgry && x.plot_size == plotsize && x.plot_view == plotview).Select(x => new { x.project_id }).ToList();
                string pass = prjid[0].ToString();
                char[] lst = { '=', ' ' };
                String[] ps = pass.Split(lst);
                long id = Convert.ToInt64(ps[4]);

                db.new_property.Add(new Models.new_property
                {
                    project_id = id,
                    project_category = prjctgry,
                    plot_size = plotsize,
                    plot_view = plotview,
                    number_plot = numberofplots,
                    plot_price = price,
                    plot_desc = details                  
                });
                db.SaveChanges();

            }
            catch(Exception ex)
            {
                throw ex;
            }


            return Json("numberofpl", JsonRequestBehavior.AllowGet);
        }




    }
}