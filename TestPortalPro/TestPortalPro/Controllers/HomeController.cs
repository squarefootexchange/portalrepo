﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestPortalPro.Models;


namespace TestPortalPro.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]

        public ActionResult Index(usr_login objUser)
        {


            if (ModelState.IsValid)
            {
                using (directsq_proportalEntities db = new directsq_proportalEntities())
                {
                    var obj = db.usr_login.Where(a => a.email.Equals(objUser.email) && a.password.Equals(objUser.password)).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.agentid.ToString();
                        Session["UserName"] = obj.email.ToString();
                        Session["user_Role"] = obj.user_role.ToString();
                        if (obj.user_role == "Admin")
                        {

                            return RedirectToAction("../Admin/Admin");
                        }

                        else
                        {
                            return RedirectToAction("../user/userDashboard");
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Invalid Username And Password";

                    }
                }
            }
            return View(objUser);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}