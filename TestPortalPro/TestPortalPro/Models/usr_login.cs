//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestPortalPro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class usr_login
    {
        public long agentid { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string user_role { get; set; }
        public string delind { get; set; }
    }
}
