﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestPortalPro.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class directsq_proportalEntities : DbContext
    {
        public directsq_proportalEntities()
            : base("name=directsq_proportalEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<booking_property> booking_property { get; set; }
        public virtual DbSet<new_project> new_project { get; set; }
        public virtual DbSet<new_property> new_property { get; set; }
        public virtual DbSet<signup> signups { get; set; }
        public virtual DbSet<sold_property> sold_property { get; set; }
        public virtual DbSet<usr_login> usr_login { get; set; }
    }
}
